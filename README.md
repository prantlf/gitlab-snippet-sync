# gitlab-snippet-sync

[![NPM version](https://badge.fury.io/js/gitlab-snippet-sync.png)](http://badge.fury.io/js/gitlab-snippet-sync)
[![Dependency Status](https://david-dm.org/prantlf/gitlab-snippet-sync.svg)](https://david-dm.org/prantlf/gitlab-snippet-sync)
[![devDependency Status](https://david-dm.org/prantlf/gitlab-snippet-sync/dev-status.svg)](https://david-dm.org/prantlf/gitlab-snippet-sync#info=devDependencies)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

[![NPM Downloads](https://nodei.co/npm/gitlab-snippet-sync.png?downloads=true&stars=true)](https://www.npmjs.com/package/gitlab-snippet-sync)

Downloads snippets from a GitLab server to a local directory.

    Usage: gitlab-snippet-sync [options]

    Options:
      -V, --version                output the version number
      -s, --server <server>        origin of the GitLab server
      -t, --token <token>          (API) access token for reading the snippets
      -n, --token-variable <name>  environment variable with the access token
      -d, --directory <directory>  work in a specific directory
      -q, --quiet                  suppress logging on the console
      -v, --verbose                log more progress on the console
      -h, --help                   output usage information

      Snippets will be downloaded to the directory "snippets". It will be
      created in the directory where this script is executed by default.
      The default server is "https://gitlab.com". The default environment
      variable with the access token is "GITLAB_TOKEN".

    Examples:

      $ GITLAB_TOKEN="..." gitlab-snippet-sync
      $ glsnipsync -s https://gitlab.my.com -t "..."

## Installation

This module can be installed globally using [NPM] or [Yarn], so that the command-line script can be executed in any directory. Make sure, that you use [Node.js] version 10 or newer.

```sh
$ npm i -g gitlab-snippet-sync
```

```sh
$ yarn add global gitlab-snippet-sync
```

## API

The module can be installed in a local project instead of globally. The functionality can be called programmatically and without the console output:

```js
const { downloadSnippets } = require('gitlab-snippet-sync')
await downloadSnippets({
  server: 'https://gitlab.com',
  token: process.env.GITLAB_TOKEN,
  directory: '.',
  quiet: true,
  verbose: false
})
```

## Contributing

In lieu of a formal styleguide, take care to maintain the existing coding style.  Add unit tests for any new or changed functionality. Lint and test your code using Grunt.

## Release History

* 2019-10-01   v0.1.2   Download only modified scripts
* 2019-09-30   v0.0.1   Initial release

## License

Copyright (c) 2019 Ferdinand Prantl

Licensed under the MIT license.

[Node.js]: http://nodejs.org/
[NPM]: https://www.npmjs.com/
[Yarn]: https://yarnpkg.com/
[CLDR plural rules]: http://cldr.unicode.org/index/cldr-spec/gitlab-snippet-sync
[Mozilla plural rules]: https://developer.mozilla.org/en-US/docs/Mozilla/Localization/Localization_and_Plurals#List_of_Plural_Rules
[fast-gitlab-snippet-sync]: https://github.com/prantlf/fast-gitlab-snippet-sync
