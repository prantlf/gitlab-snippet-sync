const http = require('http')
const https = require('https')
const { createWriteStream } = require('fs')
const decompressResponse = require('decompress-response')
const { streamToStream } = require('./stream-util')
const { inform, trace } = require('./log-util')

function openResource (url, headers = {}) {
  return new Promise(function (resolve, reject) {
    trace('Loading "%s"...', url)
    const protocol = url.startsWith('http:') ? http : https
    headers = Object.assign({
      'Accept-Encoding': 'br;q=1.0, gzip;q=0.9, deflate;q=0.8'
    }, headers)
    const request = protocol
      .get(url, { headers })
      .on('response', function (response) {
        const { statusCode, statusMessage, headers } = response
        if (statusCode === 200) {
          resolve(decompressResponse(response))
        } else if (statusCode >= 300 && statusCode < 400) {
          delete headers.host
          response.resume()
          const url = headers.location
          trace('Redirecting to "%s"...', url)
          resolve(openResource(url, headers))
        } else {
          reject(new Error(`${statusCode} ${statusMessage}`))
        }
      })
      .on('timeout', function () {
        request.abort()
        reject(new Error('Request timed out.'))
      })
      .on('error', reject)
  })
}

async function downloadFile (url, filePath) {
  inform('Writing "%s"...', filePath)
  const fileStream = createWriteStream(filePath)
  const responseStream = await openResource(url)
  await streamToStream(responseStream, fileStream)
}

module.exports = { openResource, downloadFile }
