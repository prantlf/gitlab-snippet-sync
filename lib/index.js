const { openResource, downloadFile } = require('./url-util')
const { streamToString, stringToStream, streamToStream } = require('./stream-util')
const { configureLog, inform, trace } = require('./log-util')
const { checkFile, makeDirectory } = require('./fs-util')
const { createReadStream, createWriteStream } = require('fs')

async function downloadSnippetList (server, token) {
  const responseStream = await openResource(
    `${server}/api/v4/snippets`, { 'Private-Token': token })
  return JSON.parse(await streamToString(responseStream))
}

async function readSnippetList (directory) {
  const filePath = `${directory}/snippets.json`
  if (await checkFile(filePath)) {
    trace('Reading "%s"...', filePath)
    const fileStream = createReadStream(filePath)
    return JSON.parse(await streamToString(fileStream))
  }
  return []
}

async function writeSnippetList (snippets, directory) {
  const inputStream = stringToStream(JSON.stringify(snippets, undefined, 2))
  const filePath = `${directory}/snippets.json`
  inform('Writing "%s"...', filePath)
  const outputStream = createWriteStream(filePath)
  await streamToStream(inputStream, outputStream)
}

function updateSnippet (localSnippets, snippet) {
  const localSnippet = localSnippets[snippet.id]
  if (!localSnippet) {
    return true
  }
  if (localSnippet.updated_at !== snippet.updated_at) {
    return true
  }
  trace('Skipping "%s"...', snippet.title)
}

function snippetsToMap (snippets) {
  return snippets.reduce(function (result, snippet) {
    result[snippet.id] = snippet
    return result
  }, {})
}

async function downloadSnippetCode (server, localSnippets, remoteSnippets, directory) {
  let updated
  await makeDirectory(`${directory}/snippets`)
  localSnippets = snippetsToMap(localSnippets)
  for (const snippet of remoteSnippets) {
    if (updateSnippet(localSnippets, snippet)) {
      let { id, file_name: fileName, title } = snippet
      if (!fileName) {
        fileName = title
      }
      await downloadFile(`${server}/snippets/${id}/raw`,
        `${directory}/snippets/${fileName}`)
      updated = true
    }
  }
  return updated
}

async function downloadSnippets ({ server, token, directory, quiet, verbose }) {
  configureLog({ quiet, verbose })
  const localSnippets = await readSnippetList(directory)
  const remoteSnippets = await downloadSnippetList(server, token)
  if (await downloadSnippetCode(server, localSnippets, remoteSnippets, directory)) {
    await writeSnippetList(remoteSnippets, directory)
    inform('Done.')
  } else {
    inform('No changes found.')
  }
}

module.exports = { downloadSnippets }
