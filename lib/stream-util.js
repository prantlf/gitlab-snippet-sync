const { Readable } = require('stream')

async function streamToString (inputStream) {
  return new Promise(function (resolve, reject) {
    let chunks = ''
    inputStream.setEncoding('utf8')
    inputStream
      .on('error', reject)
      .on('data', function (chunk) {
        chunks += chunk
      })
      .on('end', function () {
        resolve(chunks)
      })
  })
}

function stringToStream (inputString) {
  var outputStream = new Readable()
  outputStream.push(inputString)
  outputStream.push(null)
  return outputStream
}

async function streamToStream (inputStream, outputStream) {
  return new Promise(function (resolve, reject) {
    outputStream
      .on('error', reject)
      .on('finish', function () {
        outputStream.close(resolve)
      })
    inputStream
      .on('error', reject)
      .pipe(outputStream)
      .on('error', reject)
  })
}

module.exports = { streamToString, stringToStream, streamToStream }
