const { format } = require('util')
let quiet, verbose

function configureLog (options) {
  ({ quiet, verbose } = options)
}

function log (message, ...args) {
  const text = format(message, ...args)
  console.log(text)
}

function inform (message, ...args) {
  if (!quiet) {
    log(message, ...args)
  }
}

function trace (message, ...args) {
  if (verbose) {
    log(message, ...args)
  }
}

module.exports = { configureLog, inform, trace }
