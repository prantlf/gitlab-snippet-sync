const makeDir = require('make-dir')
const { access } = require('fs')
const { promisify } = require('util')
const { trace } = require('./log-util')

const accessAsync = promisify(access)

async function checkFile (filePath) {
  trace('Checking existence of "%s"...', filePath)
  try {
    await accessAsync(filePath)
    return true
  } catch (error) {
    if (error.code !== 'ENOENT') {
      throw error
    }
  }
}

module.exports = { checkFile, makeDirectory: makeDir }
